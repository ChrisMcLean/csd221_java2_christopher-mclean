/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSD221_Lab3Example;

/**
 *
 * @author student
 */
public class App {
    static void run(){
        
        Person p1 = new Person();
        System.out.println(p1);
        
        Person p2 = new Person("Chris", "McLean");
        System.out.println(p2);
        
        Employee e1 = new Employee("Sault College");
        System.out.println(e1);
        
        Employee e2 = new Employee("Quinten", "Tarentino", "Hollywood");
        System.out.println(e2);
        
        Employee e3 = new Employee();
        System.out.println(e3);
        
    }
}
