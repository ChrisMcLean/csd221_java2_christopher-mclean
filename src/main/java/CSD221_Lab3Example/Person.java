/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CSD221_Lab3Example;

/**
 *
 * @author student
 */
public class Person {

    private String firstname;
    private String lastname;

    public Person() {
        System.out.println("calling Person() constructor");
        firstname = "<default first name";
        lastname = "<default last name";

    }

    public Person(String firstname, String lastname) {
        System.out.println("calling Person(String firstname, String lastname))");
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {

        this.lastname = lastname;
    }

    @Override
    public String toString() {

        return "Person{" + "firstname=" + firstname + ", lastname=" + lastname + '}';
    }

}
