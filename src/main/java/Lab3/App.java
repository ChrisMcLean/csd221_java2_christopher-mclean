/* Title: Lab3
Author: Christopher McLean
Date: Jan 29, 2020
Professor: Fred Carrella
 */
package Lab3;

import java.util.Scanner;

public class App {

    //initializes the lists and scanner
    private final int numBooks = 3000;
    private Book[] books = new Book[numBooks];
    private Book[] magazine = new Book[numBooks];
    private int currentIndex = 0;
    private static Scanner input = new Scanner(System.in);
    private String menu = "MENU"
            + "-------------------\n"
            + "1. To Kill A Mockingbird\n"
            + "2. Davinci Code\n"
            + "3. Add a Book\n"
            + "4. Edit a Book\n"
            + "5. Delete a Book\n"
            + "6. List Books"
            + "99. Quit\n";

    public App() {

    }

    public void run() {

        boolean Quit = false;
        //switch statement for the menu
        while (!Quit) {
            try {
                System.out.println(menu);
                int choice = input.nextInt();

                switch (choice) {
                    case 1:
                        KillMockingbird();
                        break;
                    case 2:
                        DavinciCode();
                        break;
                    case 3:
                        Add();
                        break;
                    case 4:
                        Edit();
                        break;
                    case 5:
                        Delete();
                        break;
                    case 6:
                        List();
                        break;
                    case 99:
                        System.out.println("Exiting Application");
                        Quit = true;
                        break;
                    default:
                        System.out.println("Invalid Entry, Select a valid entry.");
                }
            } catch (Exception e) {
                System.out.println("Invalid Entry, Select a valid entry.");
                input.next();
            }
        }

        System.exit(0);
    }

    //displays the credentials for the To Kill a Mockingird book
    private static void KillMockingbird() {
        Book TKM = new Book("To Kill a Mockinbird", "Harper Lee", 15.99, 0);
        System.out.println(TKM);
    }

    //displays the credentials for the Davinci Code book
    private static void DavinciCode() {
        Book DC = new Book("Davinci Code", "Dan Brown", 25.99, 20);
        System.out.println(DC);
    }

    //Add a book to the list books
    private void Add() throws Exception {
        System.out.println("\nAdd a Book");
        System.out.println("----------------------------");
        try {
            Scanner input = new Scanner(System.in);

            System.out.println("Enter Title ('q' to Quit)");
            String title = input.next();

            if (input.next().contains("q")) {
                System.exit(0);
            } else {
                System.out.println("Enter Author");
                String author = input.next();
                System.out.println("Enter Price");
                int price = input.nextInt();
                System.out.println("Enter Number of Copies");
                int copies = input.nextInt();

                Book book = new Book(title, author, price, copies);
                books[currentIndex] = book;
                currentIndex++;
            }
        } catch (Exception e) {
            throw new Exception("Error Adding a book", e);
        }

    }

    //Edit an existiing book from the list books
    private void Edit() {
        List();

        System.out.println("\nEdit a Book");
        System.out.println("----------------------------");
        System.out.println("Which Book would you like to edit?");
        Scanner select = new Scanner(System.in);
        int s = select.nextInt() - 1;

        System.out.println("\nEnter Title");
        String title = input.next();
        System.out.println("Enter Author");
        String author = input.next();
        System.out.println("Enter Price");
        int price = input.nextInt();
        System.out.println("Enter Copies");
        int copies = input.nextInt();

        Book book = new Book(title, author, price, copies);
        books[s] = book;
    }

    //Delete a book from the list books
    private void Delete() {
        List();

        System.out.println("\nDelete a Book");
        System.out.println("----------------------------");
        for (int s = input.nextInt() - 1; s < books.length; s++) {
            if (s + 1 < books.length) {
                books[s] = books[s + 1];
                if (s + 1 == books.length || books[s + 1] == null) {
                    break;
                }
            } else {
                books[s] = null;
            }
            currentIndex--;
        }
    }

    //display the list of books in the list books
    private void List() {
        System.out.println("\nList of Books");
        System.out.println("----------------------------");
        int count = 1;

        for (Book book : books) {
            if (book == null) {
                break;
            }
            System.out.println(count++ + ". " + book);
        }
        System.out.println("-------------------\n");
    }
}

