/* Title: Lab3
Author: Christopher McLean
Date: Jan 29, 2020
Professor: Fred Carrella
 */
package Lab3;

//book class that extends from the Publication class
public class Book extends Publication {

    private String author;

    public Book() {
    }

    public Book(String title, String author, double price, int copies) {
        super(title, price, copies);
        this.author = author;
    }

    public void orderCopies(int num2Order) {
        setCopies(num2Order + getCopies());
    }

    //created a toString(0 method for the first and secont oprions of the menu
    @Override
    public String toString() {
        return "Title: " + title + " \n" + "Author: " + author + " \n" + "Price: $" + price + " \n" + "Number of Codies Sold: " + copies + " \n";
    }

}
