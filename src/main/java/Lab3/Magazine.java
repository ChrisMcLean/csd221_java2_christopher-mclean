/* Title: Lab3
Author: Christopher McLean
Date: Jan 29, 2020
Professor: Fred Carrella
 */
package Lab3;

import java.util.Date;

//magazine class, functions similar to the Book class, not used yet
public class Magazine extends Publication {

    private int orderQty;
    private Date currIssue;

    public Magazine(String title, int price, int copies) {
        this();
        setTitle(title);
        setPrice(price);
        setCopies(copies);
    }

    public Magazine() {
        orderQty = 10;
    }

    /**
     * @return the orderQty
     */
    public int getOrderQty() {
        return orderQty;
    }

    /**
     * @param orderQty the orderQty to set
     */
    public void setOrderQty(Integer orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * @return the currIssue
     */
    public Date getCurrIssue() {
        return currIssue;
    }

    /**
     * @param currIssue the currIssue to set
     */
    public void setCurrIssue(Date currIssue) {
        this.currIssue = currIssue;
    }

}
