/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;

import java.util.Scanner;

/**
 * Christopher McLean
 *
 * @author student
 */
public class App {
    //initializes the lists and scanner

    private final int numItem = 3000;
    private final Book[] books = new Book[numItem];
    private final Magazine[] magazines = new Magazine[numItem];
    private final Ticket[] tickets = new Ticket[numItem];
    private int currentIndex = 0;
    private static Scanner input = new Scanner(System.in);
    private final String mainMenu = "MENU"
            + "-------------------\n"
            + "1. Book\n"
            + "2. Ticket\n"
            + "3. Magazine\n"
            + "99. Quit\n";
    private final String bookMenu = "MENU"
            + "-------------------\n"
            + "1. To Kill A Mockingbird\n"
            + "2. Davinci Code\n"
            + "3. Add a Book\n"
            + "4. Edit a Book\n"
            + "5. Delete a Book\n"
            + "6. List Books"
            + "99. Quit\n";
    private final String ticketMenu = "MENU"
            + "-------------------\n"
            + "1. Add a Ticket\n"
            + "2. Edit a Ticket\n"
            + "3. Delete a Ticket\n"
            + "4. List Tickets"
            + "99. Quit\n";
    private final String magMenu = "MENU"
            + "-------------------\n"
            + "1. Add a Magazine\n"
            + "2. Edit a Magazine\n"
            + "3. Delete a Magazine\n"
            + "4. List Magazines"
            + "99. Quit\n";

    public App() {

    }

    public void run() {

        boolean Quit = false;
        //switch statement for the menu
        while (!Quit) {
            try {
                System.out.println(mainMenu);
                int choice = input.nextInt();

                switch (choice) {
                    case 1:
                        System.out.println(bookMenu);
                        break;
                    case 2:
                        System.out.println(ticketMenu);
                        break;
                    case 3:
                        System.out.println(magMenu);
                        break;
                    default:
                        System.out.println("Please make a selection");
                        ;
                }
                if (choice == 1) {

                    switch (choice) {
                        case 1:
                            KillMockingbird();
                            break;
                        case 2:
                            DavinciCode();
                            break;
                        case 3:
                            Add();
                            break;
                        case 4:
                            Edit();
                            break;
                        case 5:
                            Delete();
                            break;
                        case 6:
                            ListBook();
                            break;
                        case 99:
                            System.out.println("Exiting Application");
                            Quit = true;
                            break;
                        default:
                            System.out.println("Invalid Entry, Select a valid entry.");
                    }
                    if (choice == 2) {
                        switch (choice) {
                            case 1:
                                AddTicket();
                                break;
                            case 2:
                                EditTicket();
                                break;
                            case 3:
                                DeleteTicket();
                                break;
                            case 5:
                                ListTicket();
                                break;
                            case 99:
                                System.out.println("Exiting Application");
                                Quit = true;
                                break;
                            default:
                                System.out.println("Invalid Entry, Select a valid entry.");
                        }
                        if (choice == 3) {
                            switch (choice) {
                                case 1:
                                    AddMag();
                                    break;
                                case 2:
                                    EditMag();
                                    break;
                                case 3:
                                    DeleteMag();
                                    break;
                                case 5:
                                    ListMag();
                                    break;
                                case 99:
                                    System.out.println("Exiting Application");
                                    Quit = true;
                                    break;
                                default:
                                    System.out.println("Invalid Entry, Select a valid entry.");
                            }
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Invalid Entry, Select a valid entry.");
                input.next();
            }
        }

        System.exit(0);
    }

    //displays the credentials for the To Kill a Mockingird book
    private static void KillMockingbird() {
        Book TKM = new Book("To Kill a Mockinbird", "Harper Lee", 15.99, 0);
        System.out.println(TKM);
    }

    //displays the credentials for the Davinci Code book
    private static void DavinciCode() {
        Book DC = new Book("Davinci Code", "Dan Brown", 25.99, 20);
        System.out.println(DC);
    }

    //Add a book to the list books
    private void Add() throws Exception {
        System.out.println("\nAdd a Book");
        System.out.println("----------------------------");
        try {
            Scanner input = new Scanner(System.in);

            System.out.println("Enter Title ('q' to Quit)");
            String title = input.next();

            if (input.next().contains("q")) {
                System.exit(0);
            } else {
                System.out.println("Enter Author");
                String author = input.next();
                System.out.println("Enter Price");
                int price = input.nextInt();
                System.out.println("Enter Number of Copies");
                int copies = input.nextInt();

                Book book = new Book(title, author, price, copies);
                books[currentIndex] = book;
                currentIndex++;

            }
        } catch (Exception e) {
            throw new Exception("Error Adding a book", e);
        }

    }

    //Edit an existiing book from the list books
    private void Edit() {
        ListBook();

        System.out.println("\nEdit a Book");
        System.out.println("----------------------------");
        System.out.println("Which Book would you like to edit?");
        Scanner select = new Scanner(System.in);
        int s = select.nextInt() - 1;

        System.out.println("\nEnter Title");
        String title = input.next();
        System.out.println("Enter Author");
        String author = input.next();
        System.out.println("Enter Price");
        int price = input.nextInt();
        System.out.println("Enter Copies");
        int copies = input.nextInt();

        Book book = new Book(title, author, price, copies);
        books[s] = book;
    }

    //Delete a book from the list books
    private void Delete() {
        ListBook();

        System.out.println("\nDelete a Book");
        System.out.println("----------------------------");
        for (int s = input.nextInt() - 1; s < books.length; s++) {
            if (s + 1 < books.length) {
                books[s] = books[s + 1];
                if (s + 1 == books.length || books[s + 1] == null) {
                    break;
                }
            } else {
                books[s] = null;
            }
            currentIndex--;
        }
    }

    //display the list of books in the list books
    private void ListBook() {
        System.out.println("\nList of Books");
        System.out.println("----------------------------");
        int count = 1;

        for (Book book : books) {
            if (book == null) {
                break;
            }
            System.out.println(count++ + ". " + book);
        }
        System.out.println("-------------------\n");
    }

    //Magazine methods CRUD ------------------------------------>
    //Add a magazine to the list magazine
    private void AddMag() throws Exception {
        System.out.println("\nAdd a Magazine");
        System.out.println("----------------------------");
        try {
            Scanner input = new Scanner(System.in);

            System.out.println("Enter Title ('q' to Quit)");
            String title = input.next();

            if (input.next().contains("q")) {
                System.exit(0);
            } else {
                System.out.println("Enter Author");
                String author = input.next();
                System.out.println("Enter Price");
                int price = input.nextInt();
                System.out.println("Enter Number of Copies");
                int copies = input.nextInt();

                Magazine magazine = new Magazine(title, author, price, copies);
                magazines[currentIndex] = magazine;
                currentIndex++;

            }
        } catch (Exception e) {
            throw new Exception("Error Adding a Magazine", e);
        }

    }

    //Edit an existiing magazine from the list magazine
    private void EditMag() {
        ListMag();

        System.out.println("\nEdit a Magazine");
        System.out.println("----------------------------");
        System.out.println("Which Magazine would you like to edit?");
        Scanner select = new Scanner(System.in);
        int s = select.nextInt() - 1;

        System.out.println("\nEnter Title");
        String title = input.next();
        System.out.println("Enter Author");
        String author = input.next();
        System.out.println("Enter Price");
        int price = input.nextInt();
        System.out.println("Enter Copies");
        int copies = input.nextInt();

        Magazine magazine = new Magazine(title, author, price, copies);
        magazines[s] = magazine;
    }

    //Delete a magazine from the list magazine
    private void DeleteMag() {
        ListMag();

        System.out.println("\nDelete a Magazine");
        System.out.println("----------------------------");
        for (int s = input.nextInt() - 1; s < magazines.length; s++) {
            if (s + 1 < magazines.length) {
                magazines[s] = magazines[s + 1];
                if (s + 1 == magazines.length || magazines[s + 1] == null) {
                    break;
                }
            } else {
                magazines[s] = null;
            }
            currentIndex--;
        }
    }

    //display the list of magazine in the list magazine
    private void ListMag() {
        System.out.println("\nList of Magazines");
        System.out.println("----------------------------");
        int count = 1;

        for (Magazine magazine : magazines) {
            if (magazine == null) {
                break;
            }
            System.out.println(count++ + ". " + magazine);
        }
        System.out.println("-------------------\n");
    }
    
     //Ticket methods CRUD ------------------------------------>
    //Add a ticket to the list ticket
    private void AddTicket() throws Exception {
        System.out.println("\nAdd a Ticket");
        System.out.println("----------------------------");
        try {
            Scanner input = new Scanner(System.in);

            System.out.println("Enter Desciption('q' to Quit)");
            String description = input.next();

            if (input.next().contains("q")) {
                System.exit(0);
            } else {                
                System.out.println("Enter Price");
                int price = input.nextInt();
                System.out.println("Enter Clients Name");
                String client = input.next();

                Ticket ticket = new Ticket(description, price, client);
                tickets[currentIndex] = ticket;
                currentIndex++;

            }
        } catch (Exception e) {
            throw new Exception("Error Adding a Ticket", e);
        }

    }

    //Edit an existiing ticket from the list Ticket
    private void EditTicket() {
        ListTicket();

        System.out.println("\nEdit a Ticket");
        System.out.println("----------------------------");
        System.out.println("Which Ticket would you like to edit?");
        Scanner select = new Scanner(System.in);
        int s = select.nextInt() - 1;

        System.out.println("\nEnter Description");
        String description = input.next();
        System.out.println("Enter Price");
        int price = input.nextInt();
        System.out.println("Enter Copies");
        String client = input.next();
        
        Ticket ticket = new Ticket(description, price, client);
        tickets[s] = ticket;
    }

    //Delete a ticket from the list Ticket
    private void DeleteTicket() {
        ListTicket();

        System.out.println("\nDelete a Ticket");
        System.out.println("----------------------------");
        for (int s = input.nextInt() - 1; s < tickets.length; s++) {
            if (s + 1 < tickets.length) {
                tickets[s] = tickets[s + 1];
                if (s + 1 == tickets.length || tickets[s + 1] == null) {
                    break;
                }
            } else {
                tickets[s] = null;
            }
            currentIndex--;
        }
    }

    //display the list of magazine in the list magazine
    private void ListTicket() {
        System.out.println("\nList of Tickets");
        System.out.println("----------------------------");
        int count = 1;

        for (Ticket ticket : tickets) {
            if (ticket == null) {
                break;
            }
            System.out.println(count++ + ". " + ticket);
        }
        System.out.println("-------------------\n");
    }

}
