/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;

/**
 *Christopher McLean
 * @author student
 */
//book class that extends from the Publication class
public class Book extends Publication {

    private String author;
    private String to_Kill_a_Mockinbird;

    public Book() {
    }
    
    public void pencil(){
        System.out.println("Pencil Purchased!");                
    }

    public Book(String title, String author, double price, int copies) {
        super(title, price, copies);
        this.author = author;
    }

    Book(String title, String author, int price, int copies) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
 

    public void orderCopies(int num2Order) {
        setCopies(num2Order + getCopies());
    }

    //created a toString(0 method for the first and secont oprions of the menu
    @Override
    public String toString() {
        return "Title: " + title + " \n" + "Author: " + author + " \n" + "Price: $" + price + " \n" + "Number of Codies Sold: " + copies + " \n";
    }

}
