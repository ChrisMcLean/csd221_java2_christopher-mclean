/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;

import java.util.Date;

/**
 *Christopher McLean
 * @author student
 */
public class DiscMag extends Magazine{
    public void pencil(){
        System.out.println("Pencil Purchased!");                
    }
       public DiscMag() {
        orderQty = 10;
    }

      /**
     * @return the orderQty
     */
    public int getOrderQty() {
        return orderQty;
    }

    /**
     * @param orderQty the orderQty to set
     */
    public void setOrderQty(Integer orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * @return the currIssue
     */
    public Date getCurrIssue() {
        return currIssue;
    }
}
