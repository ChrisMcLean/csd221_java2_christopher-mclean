/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;
import java.util.Date;

/**
 *Christopher McLean
 * @author student
 */
//magazine class, functions similar to the Book class, not used yet
public class Magazine extends Publication {

    public int orderQty;
    public Date currIssue;

//    public Magazine(String title, int price, int copies) {
//        this();
//        setTitle(title);
//        setPrice(price);
//        setCopies(copies);
//    }

    public void pencil(){
        System.out.println("Pencil Purchased!");                
    }
    public Magazine() {
        orderQty = 10;
    }

    Magazine(String title, String author, int price, int copies) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the orderQty
     */
    public int getOrderQty() {
        return orderQty;
    }

    /**
     * @param orderQty the orderQty to set
     */
    public void setOrderQty(Integer orderQty) {
        this.orderQty = orderQty;
    }

    /**
     * @return the currIssue
     */
    public Date getCurrIssue() {
        return currIssue;
    }

    /**
     * @param currIssue the currIssue to set
     */
    public void setCurrIssue(Date currIssue) {
        this.currIssue = currIssue;
    }

}
