/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab4;

/**
 * Christopher McLean
 *
 * @author student
 */
public class Ticket implements SaleableItem {

    private String description;
    private int price;
    private String client;

    public void pencil(){
        System.out.println("Pencil Purchased!");                
    }
    
    public Ticket(String description, int price, String client) {
        //this();
        setDescription(description);
        setPrice(price);
        setClient(client);
    }

    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    public void setPrice(int Price) {
        this.price = Price;
    }

    public String getClient() {
        return client;
    }

    /**
     * @param client to client to set
     */
    public void setClient(String client) {
        this.client = client;
    }
}
