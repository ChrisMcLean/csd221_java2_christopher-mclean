/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab5;

import Lab5.controllers.MagazineJpaController;
import Lab5.entities.Book;
import Lab5.entities.Magazine;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author student
 */
public class App {

    public void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("Chris_Mclean_Lab5_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Main.class.getName()).log(Level.INFO, "Entity Manager created ({0})", emf);
            em.getTransaction().begin();

            Book newBook = new Book();
            newBook.setAuhor("Chris McLean");

            newBook.setCopies(33);
            newBook.setTitle("Learning 101");

            em.persist(newBook);
            em.getTransaction().commit();

            Magazine newMag = new Magazine();
            newMag.setCurrIssue("March 30, 2020");
            newMag.setTitle("Hello, World");
            newMag.setPrice(19.99);

            Magazine newMag2 = new Magazine();
            newMag2.setCurrIssue("April 30, 2020");
            newMag2.setTitle("Hello, World Too");
            newMag2.setPrice(20.99);

            MagazineJpaController c = new MagazineJpaController(emf);
            c.create(newMag);
            c.create(newMag2);

//            Book book = new Book();
//            Magazine mag = new Magazine(20, "April 2020");
//
//            DiscMagazine discMag = new DiscMagazine(10, "Mar 2020", true);
//
//            book.setAuthor("Fred Carella");
//            book.setTitle("Freds Book");
//
//            mag.setOrderQty(20);
//            mag.setTitle("Freds Magazine");
//
//            discMag.setTitle("Disc Mag");
//            em.persist(book);
//            em.persist(mag);
//            em.persist(discMag);
//            em.getTransaction().commit();
//
//            List<Publication> ListOfPublications = em.createQuery("SELECT c FROM Publication c").getResultList();
//            System.out.println("List of Publications");
//            for (Publication customer : ListOfPublications) {
//                System.out.println(customer.getTitle());
//            }
//            List<Book> ListOfBooks = em.createQuery("SELECT c FROM Book c").getResultList();
//            System.out.println("List of Books");
//            for (Book customer : ListOfBooks) {
//                System.out.println(customer.getTitle());
//            }
//            List<Magazine> ListOfMagazines = em.createQuery("SELECT c FROM Magazine c").getResultList();
//            System.out.println("List of Magazines");
//            for (Magazine customer : ListOfMagazines) {
//                System.out.println(customer.getTitle());
//            }

            newMag.setTitle("ABCD");
            c.edit(newMag);
            
            List<Magazine> list = c.findMagazineEntities();
            
            for (Magazine m:list){
                System.out.println("Title = " + m.getTitle());
            }            
            
            c.destroy(newMag.getId());
            
            list = c.findMagazineEntities();

            for (Magazine m:list){
                System.out.println("Magazine Title = " + m.getTitle());
            }

        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }
    }
}
