package Lab5.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author student
 */
@Entity
public class Book extends Publication implements Serializable {

    @Basic
    private String auhor;

    public String getAuhor() {
        return auhor;
    }

    public void setAuhor(String auhor) {
        this.auhor = auhor;
    }

    @Override
    public void sellCopy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}