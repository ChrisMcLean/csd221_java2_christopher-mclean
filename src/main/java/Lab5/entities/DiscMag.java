package Lab5.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;

/**
 * @author student
 */
@Entity
public class DiscMag extends Magazine {

    @Basic
    private boolean hasDisc;

    public boolean isHasDisc() {
        return hasDisc;
    }

    public void setHasDisc(boolean hasDisc) {
        this.hasDisc = hasDisc;
    }

}