/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package W20_Lab5_Example;

import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
//import w20_lab5_example.entities.Book;
//import w20_lab5_example.entities.Magazine;
//import w20_lab5_example.entities.Publication;

/**
 *
 * @author student
 */
public class App {

    void run() {
        EntityManagerFactory emf = null;
        EntityManager em = null;

        try {
            emf = Persistence.createEntityManagerFactory("W20_LAB5_EXAMPLE_PU");
            em = emf.createEntityManager();
            Logger.getLogger(App.class.getName()).log(Level.INFO, "Entity Manager created ({0})", emf);
            em.getTransaction().begin();
            
//            Book book1=new Book();
//            book1.setAuthor("Fred Carella");
//            book1.setCopies(10);
//            book1.setPrice(25.11);
//            book1.setTitle("Java Is Awesome!!!");
            
//            em.persist(book1);
//            
//            
//            Magazine mag1=new Magazine();
//            mag1.setCopies(15);
//            mag1.setCurrIssue("Feb 2020");
//            mag1.setOrderQty(20);
//            mag1.setPrice(23.23);
//            mag1.setTitle("Fredhat Magazine");
//            
//            
//            em.persist(mag1);
//            
//            em.getTransaction().commit();
//            
//            List<Publication> listOfPublications = em.createQuery("SELECT c FROM Publication c").getResultList();
//            
//            System.out.println("List of Publications");
//            for (Publication pub : listOfPublications) {
//                System.out.println(pub.getTitle());
//            }
//            
//            List<Book> listOfBooks = em.createQuery("SELECT c FROM Book c").getResultList();
//            System.out.println("List of Books");
//            for (Book book : listOfBooks) {
//                System.out.println(book.getTitle());
//            }
//            
//            em.getTransaction().begin();
//            for (Publication pub : listOfPublications) {
//                System.out.println(pub.getTitle());
//                LocalDate d=LocalDate.now();
//                pub.setTitle(pub.getTitle()+d.toString());
//                em.merge(pub);
//            }
//            em.getTransaction().commit();
//            
//            System.out.println("List of Publications");
//            for (Publication pub : listOfPublications) {
//                System.out.println(pub.getTitle());
//            }
//            
//            em.getTransaction().begin();
//            for (Publication pub : listOfPublications) {
//                em.remove(pub);
//            }
            em.getTransaction().commit();
            
        } catch (Exception e) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }
//            if(em!=null)
//                em.close();
        }
    }

}
