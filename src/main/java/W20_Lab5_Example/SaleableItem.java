/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package W20_Lab5_Example;

/**
 *
 * @author student
 */
public interface SaleableItem {
    public void sellCopy();
    public double getPrice();
}
